-- customers that included a product from the JIRA Family in their N2N landing product combination

with jira_own as
(-- identify the landing (N2N) product combination
       select   
        email_domain,
        base_product,
        platform,
        date,
        month,
        financial_year
        from public.sale 
        where sale_type = 'New to New'
        and financial_year in ('FY2016','FY2017')

)
--find the number of customers that had the 
        select  case 
                        when platform = 'Cloud' then 'Cloud'
                        when platform in ('Server','Data Center') then 'Server'
                        else 'Other'
                end as platform,
                month,
                count(distinct email_domain) as cust_count
        from jira_own
        where base_product like '%JIRA%'
        group by 1,2
        order by 1,2
        
        ;
        
  -- customers that only had a product from JIRA Family in their N2N landing product combination

with 
jira_own as
(-- identify the landing (N2N) product combination
       select   
        email_domain,
        base_product,
        platform,
        date,
        month,
        financial_year
        from public.sale 
        where sale_type = 'New to New'
        and financial_year in ('FY2016','FY2017')
        and base_product <> 'Marketplace Addon'

),
land_own as
(
        select  email_domain, 
                count(distinct base_product) as land_count
        from    jira_own
        group by 1
)
        
--coown_prod as
  --      (
        select  month,
                platform,        
                case 
                        -- 1 product only
                        when a.land_count = 1 and b.base_product in ('HipChat') then 1
                        when a.land_count = 1 and b.base_product in ('Confluence') then 2
                        when a.land_count = 1 and b.base_product in ('JIRA') then 3
                        when a.land_count = 1 and b.base_product in ('JIRA Software') then 4
                        when a.land_count = 1 and b.base_product in ('JIRA Service Desk') then 5
                        when a.land_count = 1 and b.base_product in ('JIRA Core') then 6
                        when a.land_count = 1 and b.base_product in ('Bitbucket') then 7
                        -- Hipchat based 2 combos
                        when a.land_count = 2 and b.base_product in ('HipChat','Confluence') then 8 
                        when a.land_count = 2 and b.base_product in ('HipChat','Bitbucket') then 9
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA') then 10
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Software') then 11
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Core') then 12
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Service Desk') then 13
                        --JIRA family and Confluence only
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Confluence') then 14
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Confluence') then 15
                        when a.land_count = 2 and b.base_product in ('JIRA','Confluence') then 16
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Confluence') then 17
                        -- Hipchat based 3 product combos
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','Confluence') then 18
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA') then 19
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Software') then 20  
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Core') then 21
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk') then 22   
                        --Bitbucket based 3 product combos
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA','Confluence') then 23
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Software','Confluence') then 24
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Core','Confluence') then 25
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Service Desk','Confluence') then 26
                        -- 4 product combos
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA','Confluence') then 27
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software','Confluence') then 28
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core','Confluence') then 29
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk','Confluence') then 30                       
                        -- > 4 product combos                 
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','Confluence') then 31
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Software','JIRA Core','Confluence') then 32
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Core','JIRA Service Desk','Confluence') then 33
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'JIRA Software','Confluence') then 34
                        
                else 35
                end as coown_group,
                count(b.email_domain) as cust_count
                from land_own as a
                left join jira_own as b on a.email_domain = b.email_domain
                group by 1,2,3
                order by 1,2,3
),       
--find the number of customers that had the 
        select  case 
                        when a.platform = 'Cloud' then 'Cloud'
                        when a.platform in ('Server','Data Center') then 'Server'
                        else 'Other'
                end as platform,
                a.month,
                count(distinct a.email_domain) as cust_count
        from jira_own as a
        left join product_count as b on a.email_domain = b.email_domain
        where a.base_product like '%JIRA%'
        and b.prod_count = 1
        group by 1,2
        order by 1,2
        
        ;      
        
        select * from public.license limit 10