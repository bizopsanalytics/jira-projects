-- customers that included a product from the JIRA Family in their N2N landing product combination

with jira_own as
(-- identify the landing (N2N) product combination
       select   
        email_domain,
        base_product,
        platform,
        date,
        month,
        week,
        financial_year
        from public.sale 
        where sale_type = 'New to New'
        and financial_year in ('FY2016','FY2017')

)
--find the number of customers that had the 
        select  case 
                        when platform = 'Cloud' then 'Cloud'
                        when platform in ('Server','Data Center') then 'Server'
                        else 'Other'
                end as platform,
                month,              
                count(distinct email_domain) as cust_count
        from jira_own
        where base_product like '%JIRA%'
        group by 1,2
        order by 1,2